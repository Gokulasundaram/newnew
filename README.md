# aws.tf
variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

provider "aws" {
  region = "ap-south-1"
  access_key = "${var.AWS_ACCESS_KEY}"
  secret_key = "${var.AWS_SECRET_KEY}"
}

resource "aws_key_pair" "gokul-key" {
  key_name = "gokul-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDxQzGdG6g6DPH4EdU3cj6FhngcoWfRgmMyml8bUyl/BxKX/jywTnBuCaz5AYk33GtevvbihWEsGkbc6fyXKw4Zl8TRf+s9v+4K+5O94EOX/KgIR04rpW6WwlscWGhcMMgnAVRP2aHfHRiSoAQw8GAEC1iBJT36ERVd/XZgL1E7K7NI6c8bx6unF6e4hp9u2OzPvX6MHz9cyS6LzU+WtYt9giEIyRQrepdPa+gw9Sw7A8aW5ebCkQgImM8Xm+/VswPtAicW/rjkFh4fs2qVswAzS2rqFE5LmPXVG7OmvpTQXJgE3T8dUu2az0ng7Z1rdpnuptP473CHw9ynSJXRpNBv ubuntu@ip-172-31-14-147"
}

resource "aws_instance" "web" {
    ami = "ami-2b95a744"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.gokul-key.key_name}"
    tags {
    Name = "Test instance"
    }
}
